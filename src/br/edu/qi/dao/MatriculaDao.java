package br.edu.qi.dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import br.edu.qi.dto.Matricula;

public class MatriculaDao extends GenericDao implements IDao<Matricula>{

	private static final String INSERT = "insert into matricula values(?,?)";
	private static final String FINDQTD = "select count(id_pessoa) as qtd from matricula where id_turma = ?";
	
	@Override
	public void save(Matricula obj) throws Exception {
		try {
			executeSQL(INSERT, obj.getIdTurma(),obj.getIdPessoa());
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		
	}

	@Override
	public void delete(Matricula obj) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Matricula find(Matricula obj) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<?> findAll() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public int findQTD(Matricula obj) throws Exception {
		try {
			ResultSet resultSet =  executeQuery(FINDQTD, obj.getIdTurma());
			
			if(resultSet.next()){
				return resultSet.getInt("qtd");
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		return 0;
	}

}
