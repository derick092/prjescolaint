package br.edu.qi.dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import br.edu.qi.dto.Venda;

public class VendaDao extends GenericDao implements IDao<Venda>{

	private static final String INSERT = "insert into vendas values(?,?,?,?)";
	private static final String GETLAST = "select max(id_venda) as max from vendas";
	
	@Override
	public void save(Venda obj) throws Exception {
		try {
			executeSQL(INSERT, 0,obj.getIdPessoa(),obj.getData(),obj.getValorTotal());
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public void delete(Venda obj) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Venda find(Venda obj) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<?> findAll() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public int getLast() throws Exception{
		try {
			ResultSet resultSet = executeQuery(GETLAST);
			
			while(resultSet.next()){
				return resultSet.getInt("max");
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		return 0;
	}
}
