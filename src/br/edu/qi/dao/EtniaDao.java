package br.edu.qi.dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import br.edu.qi.dto.Etnia;

public class EtniaDao extends GenericDao implements IDao<Etnia>{

	private static final String FINDALL = "select * from etnias";
	
	@Override
	public void save(Etnia obj) throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public void delete(Etnia obj) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Etnia find(Etnia obj) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Etnia> findAll() throws Exception {
		// TODO Auto-generated method stub
		ArrayList<Etnia> listaEtnias = new ArrayList<Etnia>();
		try{
			ResultSet resultSet = executeQuery(FINDALL);
			while(resultSet.next()){
				listaEtnias.add(new Etnia(resultSet.getInt("id_etnia"), resultSet.getString("descricao")));
			}
		}catch (Exception e) {
			throw new Exception("N�o foi poss�vel carregar as etnias! " + e.getMessage());
		}
		return listaEtnias;
	}

}
