package br.edu.qi.dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import br.edu.qi.dto.Produto;

public class ProdutoDao  extends GenericDao implements IDao<Produto>{

	private static final String INSERT = "insert into produtos values(?,?,?,?)";
	private static final String FINDALL = "select * from produtos where quantidade > 0";
	private static final String UPDATE = "update produtos set quantidade = ? where id_produto = ?";
	private static final String FINDONE = "select * from produtos where quantidade > 0 and id_produto = ?";
	
	public void update(Produto obj) throws Exception{
		try {
			executeSQL(UPDATE, obj.getQuantidade(),obj.getId());
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	@Override
	public void save(Produto obj) throws Exception {
		try {
			executeSQL(INSERT, 0,obj.getQuantidade(),obj.getDescricao(),obj.getValor());
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public void delete(Produto obj) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Produto find(Produto obj) throws Exception {
		try {
			ResultSet resultSet = executeQuery(FINDONE);
			
			while(resultSet.next()){
				return new Produto(resultSet.getInt("id_produto"), resultSet.getInt("quantidade"), resultSet.getString("descricao"),
						resultSet.getFloat("valor"));
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		return null;
	}

	@Override
	public ArrayList<Produto> findAll() throws Exception {
		ArrayList<Produto> listaProdutos = new ArrayList<Produto>();
		try {
			ResultSet resultSet = executeQuery(FINDALL);
			
			while(resultSet.next()){
				listaProdutos.add(new Produto(resultSet.getInt("id_produto"), resultSet.getInt("quantidade"), resultSet.getString("descricao"),
						resultSet.getFloat("valor")));
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		return listaProdutos;
	}

}