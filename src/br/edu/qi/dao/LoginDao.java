package br.edu.qi.dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import br.edu.qi.dto.Usuario;




public class LoginDao extends GenericDao implements IDao<Usuario>{

	private static final String SELECT="select * from usuarios where Login=? and Senha=?";

	@Override
	public void save(Usuario obj) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Usuario obj) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Usuario find(Usuario obj) throws Exception {
		// TODO Auto-generated method stub
		try{
			ResultSet resultSet =  executeQuery(SELECT, obj.getLogin(), obj.getSenha());
			
			if(resultSet.next()){
				return new Usuario(resultSet.getString("Login"), resultSet.getString("Senha"));
			}
		} catch (Exception e) {
			throw new Exception("Usu�rio ou senha incorreto! "+e.getMessage());
	    }
		return null;
	}

	@Override
	public ArrayList<?> findAll() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
