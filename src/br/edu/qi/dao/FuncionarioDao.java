package br.edu.qi.dao;

import java.util.ArrayList;

import br.edu.qi.dto.Funcionario;

public class FuncionarioDao extends GenericDao implements IDao<Funcionario>{

	private static final String INSERT = "insert into funcionarios values(?,?,?,?)";
	
	@Override
	public void save(Funcionario obj) throws Exception {
		// TODO Auto-generated method stub
		try {
			executeSQL(INSERT, obj.getIdPessoa(), obj.getDtAdmissao(), obj.getCargo(),obj.getFormacao());
		} catch (Exception e) {
			throw new Exception("Erro ao salvaro o funcionario! " + e.getMessage());
		}
	}

	@Override
	public void delete(Funcionario obj) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Funcionario find(Funcionario obj) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Funcionario> findAll() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
