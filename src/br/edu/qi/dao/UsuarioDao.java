package br.edu.qi.dao;

import java.util.ArrayList;

import br.edu.qi.dto.Usuario;

public class UsuarioDao extends GenericDao implements IDao<Usuario>{

	private static final String INSERT = "insert into usuarios values(?,?,?)";

	@Override
	public void save(Usuario obj) throws Exception {
		try{
			
			executeSQL(INSERT, obj.getLogin(), obj.getSenha(), obj.getIdPessoa());
			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}	
	}
	
	@Override
	public void delete(Usuario obj) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Usuario find(Usuario obj) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Usuario> findAll() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
