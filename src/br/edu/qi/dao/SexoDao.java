package br.edu.qi.dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import br.edu.qi.dto.Sexo;

public class SexoDao extends GenericDao implements IDao<Sexo>{

	private static final String FINDALL = "select * from sexo";

	@Override
	public void save(Sexo obj) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Sexo obj) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Sexo find(Sexo obj) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Sexo> findAll() throws Exception {
		// TODO Auto-generated method stub
		ArrayList<Sexo> listaSexos = new ArrayList<Sexo>();
		try{
			ResultSet resultSet = executeQuery(FINDALL);
			while(resultSet.next()){
				listaSexos.add(new Sexo(resultSet.getInt("id_sexo"),resultSet.getString("descricao")));
			}
		}catch (Exception ex){
			throw new Exception("N�o foi poss�vel carregar os sexos:" + ex.getMessage());
		}
		return listaSexos;
	}

}
