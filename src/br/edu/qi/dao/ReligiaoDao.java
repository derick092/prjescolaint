package br.edu.qi.dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import br.edu.qi.dto.Religiao;

public class ReligiaoDao extends GenericDao implements IDao<Religiao>{

	private static final String FINDALL = "select * from religiao";
	
	@Override
	public void save(Religiao obj) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Religiao obj) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Religiao find(Religiao obj) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Religiao> findAll() throws Exception {
		// TODO Auto-generated method stub
		ArrayList<Religiao> listaReligioes = new ArrayList<Religiao>();
		try{
			ResultSet resultSet = executeQuery(FINDALL);
			while(resultSet.next()){
				listaReligioes.add(new Religiao(resultSet.getInt("id_religiao"), resultSet.getString("descricao")));
			}
		}catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		return listaReligioes;
	}

}
