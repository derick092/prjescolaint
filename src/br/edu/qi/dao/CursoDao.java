package br.edu.qi.dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import br.edu.qi.dto.Curso;

public class CursoDao extends GenericDao implements IDao<Curso>{

	private static final String INSERT = "insert into cursos values(?,?)";
	private static final String FINDALL = "select * from cursos";
	
	@Override
	public void save(Curso obj) throws Exception {
	try{
			
			executeSQL(INSERT,0, obj.getDescricao());
			
		}catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public void delete(Curso obj) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Curso find(Curso obj) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Curso> findAll() throws Exception {
ArrayList<Curso> listaCursos = new ArrayList<Curso>();
		
		try{
			
			ResultSet resultSet = executeQuery(FINDALL);
			
			while(resultSet.next()){
				listaCursos.add(new Curso(resultSet.getInt("id_curso"), resultSet.getString("descricao")));
			}
			
		}catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		
		return listaCursos;
	}

}
