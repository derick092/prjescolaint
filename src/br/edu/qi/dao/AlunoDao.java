package br.edu.qi.dao;

import java.util.ArrayList;

import br.edu.qi.dto.Aluno;

public class AlunoDao extends GenericDao implements IDao<Aluno>{

	private static final String INSERT = "insert into alunos values(?,?)";
		
	@Override
	public void save(Aluno obj) throws Exception {
		// TODO Auto-generated method stub
		try {
			executeSQL(INSERT, obj.getIdPessoa(), obj.getfPagamento());
		} catch (Exception e) {
			throw new Exception(e.getMessage()); 
		}
	}

	@Override
	public void delete(Aluno obj) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Aluno find(Aluno obj) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Aluno> findAll() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
