package br.edu.qi.dao;

import java.util.ArrayList;

import br.edu.qi.dto.ProdutoVenda;

public class ProdutoVendaDao extends GenericDao implements IDao<ProdutoVenda>{

	private static final String INSERT = "insert into produto_venda values(?,?,?,?)";
	
	@Override
	public void save(ProdutoVenda obj) throws Exception {
		try {
			executeQuery(INSERT,obj.getIdItem(),obj.getIdVenda(),obj.getQuantidade(),obj.getValorUnitario());
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		
	}

	@Override
	public void delete(ProdutoVenda obj) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ProdutoVenda find(ProdutoVenda obj) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<?> findAll() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
