package br.edu.qi.dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import br.edu.qi.dto.Turma;

public class TurmaDao extends GenericDao implements IDao<Turma>{

	private static final String INSERT = "insert into turmas values(?,?,?,?,?,?,?,?,?)";
	private static final String FINDALL = "select * from turmas";
	private static final String FINDCAP = "select capacidade from turmas where id_turma = ?";
	
	
	@Override
	public void save(Turma obj) throws Exception {
		try {
			int manha = 0;
			int tarde= 0;
			int noite= 0;
			
			if (obj.isManha())
				manha = 1;
			
			if (obj.isTarde())
				tarde = 1;
			
			if (obj.isNoite());
				noite = 1;
			
			executeSQL(INSERT, 0,obj.getIdCurso(),obj.getCapacidade(),obj.getDataFim(),obj.getDataInicio(),obj.getTema(),manha,tarde,noite);
			
		} catch (Exception e) {
			throw new Exception();
		}
		
	}

	@Override
	public void delete(Turma obj) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Turma find(Turma obj) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Turma> findAll() throws Exception {
		ArrayList<Turma> listaTurmas = new ArrayList<Turma>();
		try {
			ResultSet resultSet = executeQuery(FINDALL);
			while(resultSet.next()){
				listaTurmas.add(new Turma(resultSet.getInt("id_turma"),resultSet.getInt("id_curso"),resultSet.getInt("capacidade"),resultSet.getDate("data_fim"),resultSet.getDate("data_fim"),
						resultSet.getString("tema"),resultSet.getBoolean("manha"),resultSet.getBoolean("tarde"),resultSet.getBoolean("noite")));
			}
		} catch (Exception e) {
			throw new Exception();
		}
		return listaTurmas;
	}

	public int findCap(Turma obj) throws Exception {
		try {
			ResultSet resultSet =  executeQuery(FINDCAP, obj.getId());
			
			if(resultSet.next()){
				return resultSet.getInt("capacidade");
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		return 0;
	}
	
}
