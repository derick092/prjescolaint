package br.edu.qi.dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import br.edu.qi.dto.Deficiencia;

public class DeficienciaDao extends GenericDao implements IDao<Deficiencia>{

	private static final String INSERT = "insert into deficiencias values(?,?,?)";
	private static final String FINDALL = "select * from deficiencias";

	@Override
	public void save(Deficiencia obj) throws Exception {
		try{
			
			executeSQL(INSERT,0, obj.getDescricao(), obj.getGrau());
			
		}catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public void delete(Deficiencia obj) throws Exception {
	}

	@Override
	public Deficiencia find(Deficiencia obj) throws Exception {
		return null;
	}

	@Override
	public ArrayList<Deficiencia> findAll() throws Exception {
		ArrayList<Deficiencia> listaDeficiencias = new ArrayList<Deficiencia>();
		
		try{
			
			ResultSet resultSet = executeQuery(FINDALL);
			
			while(resultSet.next()){
				listaDeficiencias.add(new Deficiencia(resultSet.getInt("id_deficencia"), resultSet.getString("descricao"), resultSet.getInt("grau")));
			}
			
		}catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		
		return listaDeficiencias;
	}
}
