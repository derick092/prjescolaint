package br.edu.qi.dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import br.edu.qi.dto.Pessoa;

public class PessoaDao extends GenericDao implements IDao<Pessoa>{

	private static final String INSERT = "insert into pessoas values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String FINDALL = "select * from pessoas where id_pessoa > 1";
	private static final String FINDALLNOTALUNO = "select * from pessoas where id_pessoa not in(select id_pessoa from alunos);";
	private static final String FINDALLALUNO = "select * from pessoas where id_pessoa in(select id_pessoa from alunos) and id_pessoa not in (select id_pessoa from matricula) and id_pessoa > 1;";
	private static final String FINDALLNOTFUNCIONARIO = "select * from pessoas where id_pessoa not in(select id_pessoa from funcionarios);";
	
	@Override
	public void save(Pessoa obj) throws Exception {
		try {
			int manha = 0;
			int tarde= 0;
			int noite= 0;
			
			if (obj.isManha())
				manha = 1;
			
			if (obj.isTarde())
				tarde = 1;
			
			if (obj.isNoite());
				noite = 1;
			
				executeSQL(INSERT, 0,obj.getCEP(),obj.getLogradouro(),obj.getNomeSocial(),obj.getNome(),obj.getTelefone(),obj.getCelular(),obj.getPai(),obj.getMae(),
						manha,tarde,noite,obj.getIdEtnia(),obj.getIdReligiao(),obj.getIdSexo(),obj.getIdDeficiencia());

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		
	}

	@Override
	public void delete(Pessoa obj) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Pessoa find(Pessoa obj) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Pessoa> findAll() throws Exception {
		ArrayList<Pessoa> listaPessoa = new ArrayList<Pessoa>();
		
		try {
			
			ResultSet resultSet = executeQuery(FINDALL);
			
			while(resultSet.next()){
			
				listaPessoa.add(new Pessoa(resultSet.getInt("id_pessoa"),resultSet.getInt("CEP"),resultSet.getString("logradouro"),resultSet.getString("nome_social"),resultSet.getString("nome"),
						resultSet.getString("telefone"),resultSet.getString("celular"),resultSet.getString("pai"),resultSet.getString("mae"),resultSet.getBoolean("manha"),resultSet.getBoolean("tarde"),
						resultSet.getBoolean("noite"),resultSet.getInt("id_etnia"),resultSet.getInt("id_religiao"),resultSet.getInt("id_sexo"),resultSet.getInt("id_deficencia")));
						
			}
			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		
		return listaPessoa;
	}
	
	public ArrayList<Pessoa> findAllNotFuncionario() throws Exception {
		ArrayList<Pessoa> listaPessoa = new ArrayList<Pessoa>();
		
		try {
			
			ResultSet resultSet = executeQuery(FINDALLNOTFUNCIONARIO);
			
			while(resultSet.next()){
			
				listaPessoa.add(new Pessoa(resultSet.getInt("id_pessoa"),resultSet.getInt("CEP"),resultSet.getString("logradouro"),resultSet.getString("nome_social"),resultSet.getString("nome"),
						resultSet.getString("telefone"),resultSet.getString("celular"),resultSet.getString("pai"),resultSet.getString("mae"),resultSet.getBoolean("manha"),resultSet.getBoolean("tarde"),
						resultSet.getBoolean("noite"),resultSet.getInt("id_etnia"),resultSet.getInt("id_religiao"),resultSet.getInt("id_sexo"),resultSet.getInt("id_deficencia")));
						
			}
			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		
		return listaPessoa;
	}
	
	public ArrayList<Pessoa> findAllNotAluno() throws Exception {
		ArrayList<Pessoa> listaPessoa = new ArrayList<Pessoa>();
		
		try {
			
			ResultSet resultSet = executeQuery(FINDALLNOTALUNO);
			
			while(resultSet.next()){
			
				listaPessoa.add(new Pessoa(resultSet.getInt("id_pessoa"),resultSet.getInt("CEP"),resultSet.getString("logradouro"),resultSet.getString("nome_social"),resultSet.getString("nome"),
						resultSet.getString("telefone"),resultSet.getString("celular"),resultSet.getString("pai"),resultSet.getString("mae"),resultSet.getBoolean("manha"),resultSet.getBoolean("tarde"),
						resultSet.getBoolean("noite"),resultSet.getInt("id_etnia"),resultSet.getInt("id_religiao"),resultSet.getInt("id_sexo"),resultSet.getInt("id_deficencia")));
						
			}
			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		
		return listaPessoa;
	}
	
	public ArrayList<Pessoa> findAllAluno() throws Exception {
		ArrayList<Pessoa> listaPessoa = new ArrayList<Pessoa>();
		
		try {
			
			ResultSet resultSet = executeQuery(FINDALLALUNO);
			
			while(resultSet.next()){
			
				listaPessoa.add(new Pessoa(resultSet.getInt("id_pessoa"),resultSet.getInt("CEP"),resultSet.getString("logradouro"),resultSet.getString("nome_social"),resultSet.getString("nome"),
						resultSet.getString("telefone"),resultSet.getString("celular"),resultSet.getString("pai"),resultSet.getString("mae"),resultSet.getBoolean("manha"),resultSet.getBoolean("tarde"),
						resultSet.getBoolean("noite"),resultSet.getInt("id_etnia"),resultSet.getInt("id_religiao"),resultSet.getInt("id_sexo"),resultSet.getInt("id_deficencia")));
						
			}
			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		
		return listaPessoa;
	}
}
