package br.edu.qi.dto;

public class Pessoa {

	private int id;
	private int CEP;
	private String logradouro;
	private String nomeSocial;
	private String nome;
	private String telefone;
	private String celular;
	private String pai;
	private String mae;
	private boolean manha;
	private boolean tarde;
	private boolean noite;
	private int idEtnia;
	private int idReligiao;
	private int idSexo;
	private int idDeficiencia;
	
	public Pessoa(int CEP, String logradouro, String nomeSocial, String nome, String telefone, String celular, String pai, String mae,
			boolean manha, boolean tarde, boolean noite, int idEtnia,int idReligiao ,int idSexo, int idDeficiencia){
		
		this.CEP = CEP;
		this.logradouro = logradouro;
		this.nomeSocial = nomeSocial;
		this.nome = nome;
		this.telefone = telefone;
		this.celular = celular;
		this.pai = pai;
		this.mae = mae;
		this.manha = manha;
		this.tarde = tarde;
		this.noite = noite;
		this.idEtnia = idEtnia;
		this.idReligiao = idReligiao;
		this.idSexo = idSexo;
		this.idDeficiencia = idDeficiencia;
	}
	
	public Pessoa(int id, int CEP, String logradouro, String nomeSocial, String nome, String telefone, String celular, String pai, String mae,
			boolean manha, boolean tarde, boolean noite, int idEtnia,int idReligiao ,int idSexo, int idDeficiencia){
		
		this.id = id;
		this.CEP = CEP;
		this.logradouro = logradouro;
		this.nomeSocial = nomeSocial;
		this.nome = nome;
		this.telefone = telefone;
		this.celular = celular;
		this.pai = pai;
		this.mae = mae;
		this.manha = manha;
		this.tarde = tarde;
		this.noite = noite;
		this.idEtnia = idEtnia;
		this.idReligiao = idReligiao;
		this.idSexo = idSexo;
		this.idDeficiencia = idDeficiencia;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCEP() {
		return CEP;
	}
	public void setCEP(int cEP) {
		CEP = cEP;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getNomeSocial() {
		return nomeSocial;
	}
	public void setNomeSocial(String nomeSocial) {
		this.nomeSocial = nomeSocial;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getPai() {
		return pai;
	}
	public void setPai(String pai) {
		this.pai = pai;
	}
	public String getMae() {
		return mae;
	}
	public void setMae(String mae) {
		this.mae = mae;
	}
	public boolean isManha() {
		return manha;
	}
	public void setManha(boolean manha) {
		this.manha = manha;
	}
	public boolean isTarde() {
		return tarde;
	}
	public void setTarde(boolean tarde) {
		this.tarde = tarde;
	}
	public boolean isNoite() {
		return noite;
	}
	public void setNoite(boolean noite) {
		this.noite = noite;
	}
	public int getIdEtnia() {
		return idEtnia;
	}
	public void setIdEtnia(int idEtnia) {
		this.idEtnia = idEtnia;
	}
	public int getIdReligiao() {
		return idReligiao;
	}
	public void setIdReligiao(int idReligiao) {
		this.idReligiao = idReligiao;
	}
	public int getIdSexo() {
		return idSexo;
	}
	public void setIdSexo(int idSexo) {
		this.idSexo = idSexo;
	}
	public int getIdDeficiencia() {
		return idDeficiencia;
	}
	public void setIdDeficiencia(int idDeficiencia) {
		this.idDeficiencia = idDeficiencia;
	}
	

}
