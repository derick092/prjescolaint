package br.edu.qi.dto;

public class Produto {
	private int id;
	private int quantidade;
	private String descricao;
	private float valor;	
	
	public Produto(int id) {
		this.id = id;
	}
	
	
	public Produto(int id, int quantidade, float valor) {
		this.id = id;
		this.quantidade = quantidade;
		this.valor = valor;
	}
	
	public Produto(int id, int quantidade, String descricao, float valor) {
		this.id = id;
		this.quantidade = quantidade;
		this.descricao = descricao;
		this.valor = valor;
	}
	
	public Produto(int quantidade, String descricao, float valor) {
		this.quantidade = quantidade;
		this.descricao = descricao;
		this.valor = valor;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public float getValor() {
		return valor;
	}
	public void setValor(float valor) {
		this.valor = valor;
	}
	
	

	

}
