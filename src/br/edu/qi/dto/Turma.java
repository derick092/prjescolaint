package br.edu.qi.dto;

import java.util.Date;

public class Turma {

	private int id;
	private int idCurso;
	private int capacidade;
	private Date dataInicio;
	private Date dataFim;
	private String tema;
	private boolean manha;
	private boolean tarde;
	private boolean noite;
		
	
	
	public Turma(int id) {
		this.id = id;
	}

	public Turma(int id, int idCurso, int capacidade, Date dataInicio, Date dataFim, String tema, boolean manha, boolean tarde, boolean noite) {
		this.id = id;
		this.idCurso = idCurso;
		this.capacidade = capacidade;
		this.dataInicio = dataInicio;
		this.dataFim = dataFim;
		this.tema = tema;
		this.manha = manha;
		this.tarde = tarde;
		this.noite = noite;
	}
	
	public Turma(int idCurso, int capacidade, Date dataInicio, Date dataFim, String tema, boolean manha, boolean tarde, boolean noite) {
		this.idCurso = idCurso;
		this.capacidade = capacidade;
		this.dataInicio = dataInicio;
		this.dataFim = dataFim;
		this.tema = tema;
		this.manha = manha;
		this.tarde = tarde;
		this.noite = noite;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdCurso() {
		return idCurso;
	}
	public void setIdCurso(int idCurso) {
		this.idCurso = idCurso;
	}
	public int getCapacidade() {
		return capacidade;
	}
	public void setCapacidade(int capacidade) {
		this.capacidade = capacidade;
	}
	public Date getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Date getDataFim() {
		return dataFim;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public String getTema() {
		return tema;
	}
	public void setTema(String tema) {
		this.tema = tema;
	}
	public boolean isManha() {
		return manha;
	}
	public void setManha(boolean manha) {
		this.manha = manha;
	}
	public boolean isTarde() {
		return tarde;
	}
	public void setTarde(boolean tarde) {
		this.tarde = tarde;
	}
	public boolean isNoite() {
		return noite;
	}
	public void setNoite(boolean noite) {
		this.noite = noite;
	}
}
