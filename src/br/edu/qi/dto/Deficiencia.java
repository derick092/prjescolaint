package br.edu.qi.dto;

public class Deficiencia {

	private int id;
	private String descricao;
	private int grau;

	public Deficiencia(int id, String descricao, int grau){
		this.id = id;
		this.descricao = descricao;
		this.grau = grau;
	}
	
	public Deficiencia(String descricao, int grau){
		this.descricao = descricao;
		this.grau = grau;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public int getGrau() {
		return grau;
	}
	public void setGrau(int grau) {
		this.grau = grau;
	}
}
