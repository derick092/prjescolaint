package br.edu.qi.dto;

import java.util.Date;

public class Venda {

	private int id;
	private int idPessoa;
	private Date data;
	private float valorTotal;
	
	public Venda(int id, int idPessoa, Date data, float valorTotal) {
		this.id = id;
		this.idPessoa = idPessoa;
		this.data = data;
		this.valorTotal = valorTotal;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdPessoa() {
		return idPessoa;
	}
	public void setIdPessoa(int idPessoa) {
		this.idPessoa = idPessoa;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public float getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(float valorTotal) {
		this.valorTotal = valorTotal;
	}
}
