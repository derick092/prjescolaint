package br.edu.qi.dto;

public class ProdutoVenda {

	private int idItem;
	private int idVenda;
	private int quantidade;
	private double valorUnitario;

	public ProdutoVenda(int idItem, int idVenda, int quantidade, double valorUnitario) {
		super();
		this.idItem = idItem;
		this.idVenda = idVenda;
		this.quantidade = quantidade;
		this.valorUnitario = valorUnitario;
	}
	
	public double getValorUnitario() {
		return valorUnitario;
	}
	
	public void setValorUnitario(double valorUnitario) {
		this.valorUnitario = valorUnitario;
	}

	public int getIdItem() {
		return idItem;
	}
	public void setIdItem(int idItem) {
		this.idItem = idItem;
	}
	public int getIdVenda() {
		return idVenda;
	}
	public void setIdVenda(int idVenda) {
		this.idVenda = idVenda;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
}
