package br.edu.qi.dto;

public class Curso {

	private int id;
	private String descricao;
	
	public Curso(int id, String descricao) {
		super();
		this.id = id;
		this.descricao = descricao;
	}
	
	public Curso(String descricao) {
		this.descricao = descricao;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
