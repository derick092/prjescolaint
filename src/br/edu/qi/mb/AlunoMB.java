package br.edu.qi.mb;

import java.util.ArrayList;

import javax.ejb.EJB;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.servlet.http.HttpSession;

import br.edu.qi.bean.AlunoBean;
import br.edu.qi.dto.Aluno;
import br.edu.qi.dto.Pessoa;

@ManagedBean
@ViewScoped
public class AlunoMB {

	@EJB
	AlunoBean ejb;
	
	private int idPessoa;
	private String formaPagamento;
	private String msgAviso;
	private ArrayList<Pessoa> pessoas;
	
	public int getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(int idPessoa) {
		this.idPessoa = idPessoa;
	}

	public String getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(String formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public String getMsgAviso() {
		return msgAviso;
	}

	public void setMsgAviso(String msgAviso) {
		this.msgAviso = msgAviso;
	}

	public ArrayList<Pessoa> getPessoas() {
		try {
			pessoas = ejb.findAllPessoas();
			return pessoas;
		} catch (Exception e) {
			this.setMsgAviso("");
			setMessage("msgErro", e.getMessage());
			return null;
		}
	}

	private void setMessage(String objErro, String msg){
		FacesMessage message = new FacesMessage(msg);
		FacesContext.getCurrentInstance().addMessage(objErro, message);
	}
	
	private void validation() throws Exception{
		if(this.formaPagamento.trim().length() == 0)
			throw new Exception("Preencha a forma de pagamento!");
	}
	
	public void logado(ComponentSystemEvent evento){
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		
		Object objSession = session.getAttribute("Login");
		FacesContext objContext = FacesContext.getCurrentInstance();
		
		if (objSession==null){
			ConfigurableNavigationHandler confNavHandler = (ConfigurableNavigationHandler) objContext.getApplication().getNavigationHandler();
			
			confNavHandler.performNavigation("login.xhtml");
		}
	}
	
	public void novaPessoa(){
		FacesContext objContext = FacesContext.getCurrentInstance();
		ConfigurableNavigationHandler confNavHandler = (ConfigurableNavigationHandler) objContext.getApplication().getNavigationHandler();
		confNavHandler.performNavigation("pessoa.xhtml");
	}
	
	public void salvar() throws Exception{
		try {
			this.setMsgAviso("");
			this.validation();
			
			ejb.save(new Aluno(this.idPessoa, this.formaPagamento));
			
			FacesContext objContext = FacesContext.getCurrentInstance();
			ConfigurableNavigationHandler confNavHandler = (ConfigurableNavigationHandler) objContext.getApplication().getNavigationHandler();
			confNavHandler.performNavigation("index.xhtml");
			
		} catch (Exception e) {
			this.setMsgAviso(e.getMessage());
			setMessage("msgErro", "Erro ao salvar: " + e.getMessage());
		}
	}
}
