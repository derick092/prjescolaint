package br.edu.qi.mb;

import java.util.ArrayList;

import javax.ejb.EJB;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.servlet.http.HttpSession;

import br.edu.qi.bean.MatriculaBean;
import br.edu.qi.dto.Matricula;
import br.edu.qi.dto.Pessoa;
import br.edu.qi.dto.Turma;

@ManagedBean
@ViewScoped
public class MatriculaMB {

	@EJB
	MatriculaBean ejb;
	
	private int idTurma;
	private int idPessoa;
	private String msgAviso;
	private ArrayList<Turma> turmas;
	private ArrayList<Pessoa>pessoas;
	
	public int getIdTurma() {
		return idTurma;
	}
	public void setIdTurma(int idTurma) {
		this.idTurma = idTurma;
	}
	public int getIdPessoa() {
		return idPessoa;
	}
	public void setIdPessoa(int idPessoa) {
		this.idPessoa = idPessoa;
	}
	public String getMsgAviso() {
		return msgAviso;
	}
	public void setMsgAviso(String msgAviso) {
		this.msgAviso = msgAviso;
	}
	
	public ArrayList<Turma> getTurmas() {
		try {
			turmas = ejb.findAllTurmas();
			return turmas;
		} catch (Exception e) {
			this.setMsgAviso("");
			setMessage("msgErro", e.getMessage());
			return null;
		}
	}
	public ArrayList<Pessoa> getPessoas() {
	try {
			pessoas = ejb.findAllAlunos();
			return pessoas;
		} catch (Exception e) {
			this.setMsgAviso("");
			setMessage("msgErro", e.getMessage());
			return null;
		}
	}
	
	private void setMessage(String objErro, String msg){
		FacesMessage message = new FacesMessage(msg);
		FacesContext.getCurrentInstance().addMessage(objErro, message);
	}
	
	private void validation() throws Exception{
		if(idTurma <= 0)
			throw new Exception("Selecione a turma!");
		
		if(idPessoa <= 0)
			throw new Exception("Selecione a pessoa!");
		
		if (ejb.findCap(new Turma(this.idTurma)) <= ejb.findQTD(new Matricula(this.idTurma)))
			throw new Exception("Turma alcan�ou o limite m�ximo!");
	}
	
	public void logado(ComponentSystemEvent evento){
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		
		Object objSession = session.getAttribute("Login");
		FacesContext objContext = FacesContext.getCurrentInstance();
		
		if (objSession==null){
			ConfigurableNavigationHandler confNavHandler = (ConfigurableNavigationHandler) objContext.getApplication().getNavigationHandler();
			
			confNavHandler.performNavigation("login.xhtml");
		}
	}
	
	public void salvar() throws Exception{
		try{
			this.setMsgAviso("");
			this.validation();
			
			ejb.save(new Matricula(this.idTurma ,this.idPessoa));
			
			FacesContext objContext = FacesContext.getCurrentInstance();
			ConfigurableNavigationHandler confNavHandler = (ConfigurableNavigationHandler) objContext.getApplication().getNavigationHandler();
			confNavHandler.performNavigation("index.xhtml");
		} catch (Exception ex) {
			this.setMsgAviso(ex.getMessage());
			setMessage("msgErro", "Erro ao salvar: "+ex.getMessage());
		}
	}
}
