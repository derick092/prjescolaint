package br.edu.qi.mb;

import java.util.ArrayList;

import javax.ejb.EJB;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.servlet.http.HttpSession;

import br.edu.qi.bean.PessoaBean;
import br.edu.qi.dto.Deficiencia;
import br.edu.qi.dto.Etnia;
import br.edu.qi.dto.Pessoa;
import br.edu.qi.dto.Religiao;
import br.edu.qi.dto.Sexo;

@ManagedBean
@ViewScoped
public class PessoaMB {

	@EJB
	PessoaBean ejb;
	
	private String msgAviso;
	private int CEP;
	private String logradouro;
	private String nomeSocial;
	private String nome;
	private String telefone;
	private String celular;
	private String pai;
	private String mae;
	private Boolean manha;
	private Boolean tarde;
	private Boolean noite;
	private int idEtnia;
	private int idReligiao;
	private int idSexo;
	private int idDeficiencia;
	private ArrayList<Etnia> etnias;
	private ArrayList<Religiao> religioes;
	private ArrayList<Sexo> sexos;
	private ArrayList<Deficiencia> deficiencias;
	

	
	public String getMsgAviso() {
		return msgAviso;
	}

	public void setMsgAviso(String msgAviso) {
		this.msgAviso = msgAviso;
	}

	public int getCEP() {
		return CEP;
	}

	public void setCEP(int cEP) {
		CEP = cEP;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNomeSocial() {
		return nomeSocial;
	}

	public void setNomeSocial(String nomeSocial) {
		this.nomeSocial = nomeSocial;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getPai() {
		return pai;
	}

	public void setPai(String pai) {
		this.pai = pai;
	}

	public String getMae() {
		return mae;
	}

	public void setMae(String mae) {
		this.mae = mae;
	}

	public Boolean getManha() {
		return manha;
	}

	public void setManha(Boolean manha) {
		this.manha = manha;
	}

	public Boolean getTarde() {
		return tarde;
	}

	public void setTarde(Boolean tarde) {
		this.tarde = tarde;
	}

	public Boolean getNoite() {
		return noite;
	}

	public void setNoite(Boolean noite) {
		this.noite = noite;
	}

	public int getIdEtnia() {
		return idEtnia;
	}

	public void setIdEtnia(int idEtnia) {
		this.idEtnia = idEtnia;
	}

	public int getIdReligiao() {
		return idReligiao;
	}

	public void setIdReligiao(int idReligiao) {
		this.idReligiao = idReligiao;
	}

	public int getIdSexo() {
		return idSexo;
	}

	public void setIdSexo(int idSexo) {
		this.idSexo = idSexo;
	}

	public int getIdDeficiencia() {
		return idDeficiencia;
	}

	public void setIdDeficiencia(int idDeficiencia) {
		this.idDeficiencia = idDeficiencia;
	}

	public ArrayList<Etnia> getEtnias() {
		try {
			etnias = ejb.findAllEtnias();
			return etnias;
		} catch (Exception e) {
			this.setMsgAviso("");
			setMessage("msgErro", e.getMessage());
			return null;
		}
	}
	
	public ArrayList<Religiao> getReligioes() {
		try {
			religioes = ejb.findAllReglioes();
			return religioes;
		} catch (Exception e) {
			this.setMsgAviso("");
			setMessage("msgErro", e.getMessage());
			return null;
		}
	}
	
	public ArrayList<Sexo> getSexos() {
		try {
			sexos = ejb.findAllSexos();
			return sexos;
		} catch (Exception e) {
			this.setMsgAviso("");
			setMessage("msgErro", e.getMessage());
			return null;
		}
	}
	
	public ArrayList<Deficiencia> getDeficiencias() {
		try {
			deficiencias = ejb.findAllDeficiencia();
			return deficiencias;
		} catch (Exception e) {
			this.setMsgAviso("");
			setMessage("msgErro", e.getMessage());
			return null;
		}
	}
	
	private void setMessage(String objErro, String msg){
		FacesMessage message = new FacesMessage(msg);
		FacesContext.getCurrentInstance().addMessage(objErro, message);
	}
	
	private void validation() throws Exception{
			
			if(this.CEP == 0)
				throw new Exception("Preencha o CEP!");
			
			if(this.logradouro.trim().length() == 0)
				throw new Exception("Preencha o Logrdouro!");
			
			if(this.nomeSocial.trim().length()==0)
				throw new Exception("Preencha o Nome Social!");
			
			if(this.nome.trim().length()==0)
				throw new Exception("Preencha o Nome!");
			
			if (this.telefone.trim().length()==0)
				throw new Exception("Preencha o Telefone!");
			
			if (this.celular.trim().length()==0)
				throw new Exception("Preencha o Celular!");
			
			if (this.pai.trim().length()==0)
				throw new Exception("Preencha o nome do Pai!");
			
			if (this.mae.trim().length()==0)
				throw new Exception("Preencha o nome da M�e!");
			
			if(this.idEtnia < 0)
				throw new Exception("Escolha a Etnia!");
			
			if(this.idReligiao < 0)
				throw new Exception("Escolha a Religi�o!");
			
			if (this.idSexo < 0)
				throw new Exception("Escolha o Sexo!");
	}
	
	public void logado(ComponentSystemEvent evento){
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		
		Object objSession = session.getAttribute("Login");
		FacesContext objContext = FacesContext.getCurrentInstance();
		
		if (objSession==null){
			ConfigurableNavigationHandler confNavHandler = (ConfigurableNavigationHandler) objContext.getApplication().getNavigationHandler();
			
			confNavHandler.performNavigation("login.xhtml");
		}
	}
	
	public void salvar() throws Exception{
		try {
			this.setMsgAviso("");
			this.validation();
			
			Pessoa p = new Pessoa(this.CEP, this.logradouro, this.nomeSocial, this.nome, this.telefone, this.celular, this.pai, this.mae, this.manha, this.tarde, this.noite,
					this.idEtnia, this.idReligiao, this.idSexo, this.idDeficiencia);
			
			ejb.save(p);
			
			FacesContext objContext = FacesContext.getCurrentInstance();
			ConfigurableNavigationHandler confNavHandler = (ConfigurableNavigationHandler) objContext.getApplication().getNavigationHandler();
			confNavHandler.performNavigation("index.xhtml");
			
		} catch (Exception e) {
			this.setMsgAviso(e.getMessage());
			setMessage("msgErro", "Erro ao salvar: " + e.getMessage());
		}
	}
}
