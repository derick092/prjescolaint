package br.edu.qi.mb;

import java.util.ArrayList;
import java.util.Date;

import javax.ejb.EJB;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.servlet.http.HttpSession;

import br.edu.qi.bean.FuncionarioBean;
import br.edu.qi.dto.Funcionario;
import br.edu.qi.dto.Pessoa;
import br.edu.qi.dto.Usuario;

@ManagedBean
@ViewScoped
public class FuncionarioMB {

	@EJB
	FuncionarioBean ejb;
	
	private int idPessoa;
	private Date dtAdmissao;
	private String cargo;
	private String formacao;
	private String senha;
	private String resenha;
	private String login;
	private String msgAviso;

	private ArrayList<Pessoa> pessoas;
	
	public int getIdPessoa() {
		return idPessoa;
	}
	public void setIdPessoa(int idPessoa) {
		this.idPessoa = idPessoa;
	}
	public Date getDtAdmissao() {
		return dtAdmissao;
	}
	public void setDtAdmissao(Date dtAdmissao) {
		this.dtAdmissao = dtAdmissao;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getFormacao() {
		return formacao;
	}
	public void setFormacao(String formacao) {
		this.formacao = formacao;
	}

	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getResenha() {
		return resenha;
	}

	public void setResenha(String resenha) {
		this.resenha = resenha;
	}
	
	public String getMsgAviso() {
		return msgAviso;
	}
	public void setMsgAviso(String msgAviso) {
		this.msgAviso = msgAviso;
	}
	
	public ArrayList<Pessoa> getPessoas() {
		try {
			pessoas = ejb.findAllPessoas();
			return pessoas;
		} catch (Exception e) {
			this.setMsgAviso("");
			setMessage("msgErro", e.getMessage());
			return null;
		}
	}
	
	private void setMessage(String objErro, String msg){
		FacesMessage message = new FacesMessage(msg);
		FacesContext.getCurrentInstance().addMessage(objErro, message);
	}
	
	private void validation() throws Exception{
			if (this.dtAdmissao.toString().trim().length()==0)
				throw new Exception("Preecha a data de Admis�o!");
			
			if(this.cargo.trim().length()==0)
				throw new Exception("Preecha o Cargo!");
			
			if(this.formacao.trim().length()==0)
				throw new Exception("Preecha a Forma��o!");
			
			if(this.login.trim().length()==0)
				throw new Exception("Preecha o login!");
			
			if(this.senha.trim().length()==0)
				throw new Exception("Preecha a senha!");
			
			if(this.resenha.trim().length()==0)
				throw new Exception("Preecha a re senha!");
			
			if(!(this.senha.trim().equals(this.resenha.trim())))
				throw new Exception("Senhas n�o conferem!");

	}
	
	public void logado(ComponentSystemEvent evento){
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		
		Object objSession = session.getAttribute("Login");
		FacesContext objContext = FacesContext.getCurrentInstance();
		
		if (objSession==null){
			ConfigurableNavigationHandler confNavHandler = (ConfigurableNavigationHandler) objContext.getApplication().getNavigationHandler();
			confNavHandler.performNavigation("login.xhtml");
		}
	}
	
	public void novaPessoa(){
		FacesContext objContext = FacesContext.getCurrentInstance();
		ConfigurableNavigationHandler confNavHandler = (ConfigurableNavigationHandler) objContext.getApplication().getNavigationHandler();
		confNavHandler.performNavigation("pessoa.xhtml");
	}
	
	public void salvar() throws Exception{
		try {
			
			this.setMsgAviso("");
			this.validation();
			
			ejb.save(new Funcionario(this.idPessoa, this.dtAdmissao, this.cargo,this.formacao));
			ejb.saveUsuario(new Usuario(this.idPessoa, this.login, this.senha));
			
			FacesContext objContext = FacesContext.getCurrentInstance();
			ConfigurableNavigationHandler confNavHandler = (ConfigurableNavigationHandler) objContext.getApplication().getNavigationHandler();
			confNavHandler.performNavigation("index.xhtml");
		} catch (Exception e) {
			this.setMsgAviso(e.getMessage());
			setMessage("msgErro", "Erro ao salvar: " + e.getMessage());
		}
	}
	
}
