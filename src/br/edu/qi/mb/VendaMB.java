package br.edu.qi.mb;

import java.util.ArrayList;
import java.util.Date;

import javax.ejb.EJB;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.servlet.http.HttpSession;



import br.edu.qi.bean.VendaBean;
import br.edu.qi.dto.Pessoa;
import br.edu.qi.dto.Produto;
import br.edu.qi.dto.ProdutoVenda;
import br.edu.qi.dto.Venda;

@ManagedBean
@ViewScoped
public class VendaMB {
	
	@EJB
	VendaBean ejb;
	
	private int idPessoa;
	private int quantidade;
	private ArrayList<Integer> produtosCompra;
	private String msgAviso;
	private ArrayList<Produto> produtosVenda;
	private ArrayList<Pessoa> pessoas;
	
	public int getIdPessoa() {
		return idPessoa;
	}
	public void setIdPessoa(int idPessoa) {
		this.idPessoa = idPessoa;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	
	public ArrayList<Integer> getProdutosCompra() {
		return produtosCompra;
	}
	public void setProdutosCompra(ArrayList<Integer> produtosCompra) {
		this.produtosCompra = produtosCompra;
	}
	public String getMsgAviso() {
		return msgAviso;
	}
	public void setMsgAviso(String msgAviso) {
		this.msgAviso = msgAviso;
	}
	public ArrayList<Produto> getProdutosVenda() {
		try {
			produtosVenda = ejb.findAll();
			return produtosVenda;
		} catch (Exception e) {
			this.setMsgAviso("");
			setMessage("msgErro", e.getMessage());
			return null;
		}
	}	
	public ArrayList<Pessoa> getPessoas() {
		try {
			pessoas = ejb.findAllPessoas();
			return pessoas;
		} catch (Exception e) {
			this.setMsgAviso("");
			setMessage("msgErro", e.getMessage());
			return null;
		}
	}
	
	private void setMessage(String objErro, String msg){
		FacesMessage message = new FacesMessage(msg);
		FacesContext.getCurrentInstance().addMessage(objErro, message);
	}
	
	private void validation() throws Exception{
		if(this.idPessoa<=0)
			throw new Exception("Selecione pessoa!");
	
		if(this.produtosCompra==null || this.produtosCompra.isEmpty())
			throw new Exception("Selecione algum produto!");
		
		if(this.quantidade<=0)
			throw new Exception("Selecione a quantidade de produto!");
	}
	
	public void logado(ComponentSystemEvent evento){
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		
		Object objSession = session.getAttribute("Login");
		FacesContext objContext = FacesContext.getCurrentInstance();
		
		if (objSession==null){
			ConfigurableNavigationHandler confNavHandler = (ConfigurableNavigationHandler) objContext.getApplication().getNavigationHandler();
			
			confNavHandler.performNavigation("login.xhtml");
		}
	}
	
	public void salvar() throws Exception{
		try {
			this.setMsgAviso("");
			this.validation();
						
			ArrayList<Produto> prodCompras = new ArrayList<Produto>();
			for (int i : produtosCompra) {
				for (Produto pc : produtosVenda) {
					if (pc.getId() == i)
						prodCompras.add(pc);
				}
			}
			
			float val = 0;
			
			for (Produto produto : prodCompras) {
				if(produto.getQuantidade() >= this.quantidade ){
					produto.setQuantidade(produto.getQuantidade()-this.quantidade);
					val += produto.getValor();
				}else{
					throw new Exception("Quantidade maior do que dísponivel!");
				}
			}
			
			ejb.saveVenda(new Venda(0, this.idPessoa, new Date(), val));

			int lastId = ejb.getLastVenda();
			
			for (Produto produto : prodCompras) {
				ejb.saveProdutoVenda(new ProdutoVenda(produto.getId(), lastId, this.quantidade, produto.getValor()));
				ejb.updateProduto(produto);
			}
			
			FacesContext objContext = FacesContext.getCurrentInstance();
			ConfigurableNavigationHandler confNavHandler = (ConfigurableNavigationHandler) objContext.getApplication().getNavigationHandler();
			confNavHandler.performNavigation("index.xhtml");
			
		} catch (Exception e) {
			this.setMsgAviso(e.getMessage());
			setMessage("msgErro", "Erro ao salvar: " + e.getMessage());
		}
	}
	

}
