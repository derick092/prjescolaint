package br.edu.qi.mb;

import javax.ejb.EJB;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.servlet.http.HttpSession;

import br.edu.qi.bean.CursoBean;
import br.edu.qi.dto.Curso;

@ManagedBean
@ViewScoped
public class CursoMB {

	@EJB
	CursoBean ejb;
	
	private String descricao;
	private String msgAviso;
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getMsgAviso() {
		return msgAviso;
	}
	public void setMsgAviso(String msgAviso) {
		this.msgAviso = msgAviso;
	}
	
	private void setMessage(String objErro, String msg) {
		FacesMessage message = new FacesMessage(msg);
		FacesContext.getCurrentInstance().addMessage(objErro, message);
	}
	
	private void validation() throws Exception{
		if (this.descricao.trim().length()==0)
			throw new Exception("Preencha a descri��o!");
	}
	
	public void logado(ComponentSystemEvent evento){
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		
		Object objSession = session.getAttribute("Login");
		FacesContext objContext = FacesContext.getCurrentInstance();
		
		if (objSession==null){
			ConfigurableNavigationHandler confNavHandler = (ConfigurableNavigationHandler) objContext.getApplication().getNavigationHandler();
			
			confNavHandler.performNavigation("login.xhtml");
		}
	}
	
	public void salvar() throws Exception{
		try{
			this.setMsgAviso("");
			this.validation();
			
			ejb.save(new Curso(this.descricao));
			
			FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
		} catch (Exception ex) {
			this.setMsgAviso(ex.getMessage());
			setMessage("msgErro", "Erro ao salvar: "+ex.getMessage());
		}
	}
}
