package br.edu.qi.mb;

import java.util.ArrayList;
import java.util.Date;

import javax.ejb.EJB;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.servlet.http.HttpSession;

import br.edu.qi.bean.TurmaBean;
import br.edu.qi.dto.Curso;
import br.edu.qi.dto.Turma;

@ManagedBean
@ViewScoped
public class TurmaMB {

	@EJB
	TurmaBean ejb;
	
	private int idCurso;
	private int capacidade;
	private Date dataInicio;
	private Date dataFim;
	private String tema;
	private boolean manha;
	private boolean tarde;
	private boolean noite;
	private String msgAviso;
	private ArrayList<Curso> cursos;
	
	public int getIdCurso() {
		return idCurso;
	}
	public void setIdCurso(int idCurso) {
		this.idCurso = idCurso;
	}
	public int getCapacidade() {
		return capacidade;
	}
	public void setCapacidade(int capacidade) {
		this.capacidade = capacidade;
	}
	public Date getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Date getDataFim() {
		return dataFim;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public String getTema() {
		return tema;
	}
	public void setTema(String tema) {
		this.tema = tema;
	}
	public boolean isManha() {
		return manha;
	}
	public void setManha(boolean manha) {
		this.manha = manha;
	}
	public boolean isTarde() {
		return tarde;
	}
	public void setTarde(boolean tarde) {
		this.tarde = tarde;
	}
	public boolean isNoite() {
		return noite;
	}
	public void setNoite(boolean noite) {
		this.noite = noite;
	}
	public String getMsgAviso() {
		return msgAviso;
	}
	public void setMsgAviso(String msgAviso) {
		this.msgAviso = msgAviso;
	}
	public ArrayList<Curso> getCursos() {
		try {
			cursos = ejb.findAllCruso();
			return cursos;
		} catch (Exception e) {
			this.setMsgAviso("");
			setMessage("msgErro", e.getMessage());
			return null;
		}
	}
	
	private void setMessage(String objErro, String msg){
		FacesMessage message = new FacesMessage(msg);
		FacesContext.getCurrentInstance().addMessage(objErro, message);
	}
	
	private void validation() throws Exception{
		if (this.idCurso <= 0)
			throw new Exception("Selecione o curso!");
		
		if(this.capacidade <= 0)
			throw new Exception("Preencha a capacidade!");
		
		if (this.dataInicio.toString().trim().length() == 0)
			throw new Exception("Preencha data de inicio!");
		
		if (this.dataFim.toString().trim().length() == 0)
			throw new Exception("Preencha data de termino!");
		
		if (this.tema.trim().length()==0)
			throw new Exception("Preencha o tema!");
		
		if(this.manha == false && this.tarde == false & this.noite == false)
			throw new Exception("Escolha o periodo da turma!");
	}
	
	public void logado(ComponentSystemEvent evento){
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		
		Object objSession = session.getAttribute("Login");
		FacesContext objContext = FacesContext.getCurrentInstance();
		
		if (objSession==null){
			ConfigurableNavigationHandler confNavHandler = (ConfigurableNavigationHandler) objContext.getApplication().getNavigationHandler();
			
			confNavHandler.performNavigation("login.xhtml");
		}
	}
	
	public void salvar() throws Exception{
		try {
			this.setMsgAviso("");
			this.validation();
			
			ejb.save(new Turma(this.idCurso, this.capacidade, this.dataInicio, this.dataFim, this.tema, this.manha, this.tarde, this.noite));
			
			FacesContext objContext = FacesContext.getCurrentInstance();
			ConfigurableNavigationHandler confNavHandler = (ConfigurableNavigationHandler) objContext.getApplication().getNavigationHandler();
			confNavHandler.performNavigation("index.xhtml");
			
		} catch (Exception e) {
			this.setMsgAviso(e.getMessage());
			setMessage("msgErro", "Erro ao salvar: " + e.getMessage());
		}
	}
}
