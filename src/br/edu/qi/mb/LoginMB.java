package br.edu.qi.mb;

import javax.faces.bean.ManagedBean;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import br.edu.qi.bean.LoginBean;
import br.edu.qi.dto.Usuario;

@ManagedBean
@ViewScoped
public class LoginMB {

	@EJB
	LoginBean ejb;
	
	private String login;
	private String senha;
	private String msgAviso;
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getMsgAviso() {
		return msgAviso;
	}

	public void setMsgAviso(String msgAviso) {
		this.msgAviso = msgAviso;
	}

	private void setMessage(String objErro, String msg){
		FacesMessage message = new FacesMessage(msg);
		FacesContext.getCurrentInstance().addMessage(objErro, message);
	}
	
	private void validation() throws Exception{
		if(this.login.trim().length()==0){
			throw new Exception("Informe o login!");
		}
		
		if(this.senha.trim().length()==0){
			throw new Exception("Informe a senha!");
		}
	}
	
	public void logar(){
		try{
			this.setMsgAviso("");
			this.validation();
			
			Usuario usuario = new Usuario(this.login, this.senha);
			usuario = ejb.login(usuario);
			
			if(usuario != null){
				HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
				session.setAttribute("Login", usuario);
				
				FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
			}else{
				this.setMsgAviso("Usuario ou senha errados!");
			}
			
		}catch (Exception ex) {
			this.setMsgAviso("");
			setMessage("msgErro", ex.getMessage());
		}
	}
}
