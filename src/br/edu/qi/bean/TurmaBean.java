package br.edu.qi.bean;

import java.util.ArrayList;

import javax.ejb.Local;
import javax.ejb.Stateless;

import br.edu.qi.dao.CursoDao;
import br.edu.qi.dao.TurmaDao;
import br.edu.qi.dto.Curso;
import br.edu.qi.dto.Turma;

@Stateless
@Local
public class TurmaBean {

	public void save(Turma obj) throws Exception{
		try {
			new TurmaDao().save(obj);
		} catch (Exception e) {
			throw new Exception();
		}
	}
	
	public ArrayList<Curso> findAllCruso() throws Exception{
		try {
			return new CursoDao().findAll();
		} catch (Exception e) {
			throw new Exception();
		}
	}
}
