package br.edu.qi.bean;

import java.util.ArrayList;

import javax.ejb.Local;
import javax.ejb.Stateless;

import br.edu.qi.dao.FuncionarioDao;
import br.edu.qi.dao.PessoaDao;
import br.edu.qi.dao.UsuarioDao;
import br.edu.qi.dto.Funcionario;
import br.edu.qi.dto.Pessoa;
import br.edu.qi.dto.Usuario;

@Stateless
@Local
public class FuncionarioBean {

	public void save(Funcionario obj) throws Exception{
		try {
			
			new FuncionarioDao().save(obj);
			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public ArrayList<Pessoa> findAllPessoas() throws Exception{
		try {
			
			return new PessoaDao().findAllNotFuncionario();
			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public void saveUsuario(Usuario obj) throws Exception{
		try {
			new UsuarioDao().save(obj);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
}
