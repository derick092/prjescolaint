package br.edu.qi.bean;

import java.util.ArrayList;

import javax.ejb.Local;
import javax.ejb.Stateless;

import br.edu.qi.dao.MatriculaDao;
import br.edu.qi.dao.PessoaDao;
import br.edu.qi.dao.TurmaDao;
import br.edu.qi.dto.Matricula;
import br.edu.qi.dto.Pessoa;
import br.edu.qi.dto.Turma;

@Stateless
@Local
public class MatriculaBean {

	public void save(Matricula obj) throws Exception{
		try {
			new MatriculaDao().save(obj);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public ArrayList<Turma> findAllTurmas() throws Exception{
		try {
			return new TurmaDao().findAll();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public ArrayList<Pessoa> findAllAlunos() throws Exception{
		try {
			return new PessoaDao().findAllAluno();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public int findQTD(Matricula obj) throws Exception{
		try {
			return new MatriculaDao().findQTD(obj);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public int findCap(Turma obj) throws Exception{
		try {
			return new TurmaDao().findCap(obj);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
}
