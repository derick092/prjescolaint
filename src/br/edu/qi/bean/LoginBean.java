package br.edu.qi.bean;

import javax.ejb.Local;
import javax.ejb.Stateless;

import br.edu.qi.dao.LoginDao;
import br.edu.qi.dto.Usuario;

@Stateless
@Local
public class LoginBean {

	public Usuario login(Usuario obj) throws Exception{
		try{
			LoginDao loginDao = new LoginDao();
			return loginDao.find(obj);
		}catch(Exception ex){
			throw new Exception("Erro ao procurar: "+ex.getMessage());
		}
	}
}
