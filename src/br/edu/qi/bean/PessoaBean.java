package br.edu.qi.bean;

import java.util.ArrayList;

import javax.ejb.Local;
import javax.ejb.Stateless;

import br.edu.qi.dao.DeficienciaDao;
import br.edu.qi.dao.EtniaDao;
import br.edu.qi.dao.PessoaDao;
import br.edu.qi.dao.ReligiaoDao;
import br.edu.qi.dao.SexoDao;
import br.edu.qi.dto.Deficiencia;
import br.edu.qi.dto.Etnia;
import br.edu.qi.dto.Pessoa;
import br.edu.qi.dto.Religiao;
import br.edu.qi.dto.Sexo;

@Stateless
@Local
public class PessoaBean {

	public void save(Pessoa obj) throws Exception{
		try {
		
			new PessoaDao().save(obj);
			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public ArrayList<Deficiencia> findAllDeficiencia() throws Exception{
		try {
			
			return new DeficienciaDao().findAll();
			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public ArrayList<Etnia> findAllEtnias() throws Exception{
		try {
			
			return new EtniaDao().findAll();
			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public ArrayList<Religiao> findAllReglioes() throws Exception{
		try {
			
			return new ReligiaoDao().findAll();
			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public ArrayList<Sexo> findAllSexos() throws Exception{
		try {
			return new SexoDao().findAll();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
}
