package br.edu.qi.bean;


import javax.ejb.Local;
import javax.ejb.Stateless;

import br.edu.qi.dao.CursoDao;
import br.edu.qi.dto.Curso;

@Stateless
@Local
public class CursoBean {
 
	public void save(Curso obj) throws Exception{
		try {
			new CursoDao().save(obj);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
}
