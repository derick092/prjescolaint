package br.edu.qi.bean;

import java.util.ArrayList;

import javax.ejb.Local;
import javax.ejb.Stateless;

import br.edu.qi.dao.PessoaDao;
import br.edu.qi.dao.ProdutoDao;
import br.edu.qi.dao.ProdutoVendaDao;
import br.edu.qi.dao.VendaDao;
import br.edu.qi.dto.Pessoa;
import br.edu.qi.dto.Produto;
import br.edu.qi.dto.ProdutoVenda;
import br.edu.qi.dto.Venda;

@Stateless
@Local
public class VendaBean {

	public ArrayList<Produto> findAll() throws Exception{
		try {
			return new ProdutoDao().findAll();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public Produto findOne(Produto obj) throws Exception{
		try {
			return new ProdutoDao().find(obj);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public void saveProdutoVenda(ProdutoVenda obj) throws Exception{
		try {
			new ProdutoVendaDao().save(obj);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public void updateProduto(Produto obj) throws Exception{
		try {
			new ProdutoDao().update(obj);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public void saveVenda(Venda obj) throws Exception{
		try {
			new VendaDao().save(obj);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public ArrayList<Pessoa> findAllPessoas() throws Exception {
		try {
			return new PessoaDao().findAll();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public int getLastVenda() throws Exception{
		try {
			return new VendaDao().getLast();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
}
