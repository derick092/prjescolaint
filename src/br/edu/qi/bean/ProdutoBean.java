package br.edu.qi.bean;

import javax.ejb.Local;
import javax.ejb.Stateless;

import br.edu.qi.dao.ProdutoDao;
import br.edu.qi.dto.Produto;

@Stateless
@Local
public class ProdutoBean {

	public void save(Produto obj) throws Exception{
		try {
			
			new ProdutoDao().save(obj);
			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
}
