package br.edu.qi.bean;

import javax.ejb.Local;
import javax.ejb.Stateless;

import br.edu.qi.dao.DeficienciaDao;
import br.edu.qi.dto.Deficiencia;

@Stateless
@Local
public class DeficienciaBean {

	public void save(Deficiencia obj) throws Exception{
		try{
			
			DeficienciaDao deficienciaDao = new DeficienciaDao();
			deficienciaDao.save(obj);
			
		}catch(Exception ex){
			throw new Exception(ex.getMessage());
		}
	}
}
