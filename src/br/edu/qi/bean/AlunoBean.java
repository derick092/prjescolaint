package br.edu.qi.bean;

import java.util.ArrayList;

import javax.ejb.Local;
import javax.ejb.Stateless;

import br.edu.qi.dao.AlunoDao;
import br.edu.qi.dao.PessoaDao;
import br.edu.qi.dto.Aluno;
import br.edu.qi.dto.Pessoa;

@Stateless
@Local
public class AlunoBean {

	public void save(Aluno obj) throws Exception{
		try {
			
			new AlunoDao().save(obj);
			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public ArrayList<Pessoa> findAllPessoas() throws Exception{
		try {
			return new PessoaDao().findAllNotAluno();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

}
