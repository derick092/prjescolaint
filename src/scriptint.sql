CREATE DATABASE DBINT;
USE DBINT;

CREATE TABLE Etnias (
Id_etnia Int NOT NULL AUTO_INCREMENT PRIMARY KEY,
Descricao Varchar(15)
);
INSERT INTO Etnias values (0,'Caucasiano');
INSERT INTO Etnias values (0,'Negro');
INSERT INTO Etnias values (0,'Pardo');
INSERT INTO Etnias values (0,'Indigena');

CREATE TABLE Religiao (
Id_religiao Int NOT NULL AUTO_INCREMENT PRIMARY KEY,
Descricao Varchar(15)
);
INSERT INTO Religiao values (0,'Crist�o');
INSERT INTO Religiao values (0,'Judeu');
INSERT INTO Religiao values (0,'Espirita');
INSERT INTO Religiao values (0,'Ateu');

CREATE TABLE Deficiencias (
Id_deficencia Int NOT NULL AUTO_INCREMENT PRIMARY KEY,
Descricao Varchar(15),
Grau Int
);
INSERT INTO Deficiencias values (0,'Nenhuma',0);

CREATE TABLE Sexo (
Id_sexo Int NOT NULL AUTO_INCREMENT PRIMARY KEY,
Descricao Varchar(15)
);
INSERT INTO Sexo values(0,'Masculino');
INSERT INTO Sexo values(0,'Feminino');
INSERT INTO Sexo values(0,'N�o Informado');

CREATE TABLE Pessoas (
Id_pessoa Int NOT NULL AUTO_INCREMENT PRIMARY KEY,
CEP Int,
Logradouro Varchar(70),
Nome_social Varchar(50),
Nome Varchar(50),
Telefone Varchar(50),
Celular Varchar (50),
Pai Varchar(50),
Mae Varchar(50),
Manha BIT,
Tarde BIT,
Noite BIT,
Id_etnia Int,
Id_religiao Int,
Id_sexo Int,
Id_deficencia int,
FOREIGN KEY(Id_etnia) REFERENCES Etnias (Id_etnia),
FOREIGN KEY(Id_religiao) REFERENCES Religiao (Id_religiao),
FOREIGN KEY(Id_deficencia) REFERENCES Deficiencias (Id_deficencia),
FOREIGN KEY(Id_sexo) REFERENCES Sexo (Id_sexo)
);
INSERT INTO Pessoas values (0,0,'0','Administrador','Admin','0','0','Ad','Min',0,0,0,1,1,1,1);

CREATE TABLE Funcionarios (
Id_pessoa Int NOT NULL AUTO_INCREMENT PRIMARY KEY,
Dt_admissao Datetime,
Cargo Varchar(15),
Formacao Varchar(50)
);
INSERT INTO Funcionarios values(1,'2015-01-01','FUCKING BOSS','FUCKING BOSS');

CREATE TABLE Alunos (
Id_pessoa Int NOT NULL AUTO_INCREMENT PRIMARY KEY,
F_pagamento Varchar(15),
FOREIGN KEY(Id_pessoa) REFERENCES Pessoas (Id_pessoa)
);
INSERT INTO Alunos values(1,'0');


CREATE TABLE Usuarios (
Login Varchar(15) PRIMARY KEY,
Senha Varchar(15),
Id_pessoa Int,
FOREIGN KEY(Id_pessoa) REFERENCES Pessoas (Id_pessoa)
);
INSERT INTO Usuarios VALUES ('admin','admin',1);

CREATE TABLE Vendas (
Id_venda Int NOT NULL AUTO_INCREMENT PRIMARY KEY,
Id_pessoa Int,
Data Datetime,
Valor_total Float,
FOREIGN KEY(Id_pessoa) REFERENCES Pessoas (Id_pessoa)
);

CREATE TABLE Cursos (
Id_curso Int NOT NULL AUTO_INCREMENT PRIMARY KEY,
Descricao Varchar(15)
);

CREATE TABLE Turmas (
Id_turma Int NOT NULL AUTO_INCREMENT PRIMARY KEY,
Id_curso Int,
Capacidade Int,
Data_fim Datetime,
Data_inicio Datetime,
Tema Varchar(70),
Manha BIT,
Tarde BIT,
Noite BIT,
FOREIGN KEY(Id_curso) REFERENCES Cursos (Id_curso)
);


CREATE TABLE Produtos (
id_produto Int NOT NULL AUTO_INCREMENT PRIMARY KEY,
Quantidade Int,
Descricao Varchar(70),
Valor Float
);

CREATE TABLE produto_venda (
id_produto Int,
Id_venda Int,
Quantidade Int,
Valor_unitario float,
PRIMARY KEY(id_produto,Id_venda),
FOREIGN KEY(id_produto) REFERENCES Produtos (id_produto),
FOREIGN KEY(Id_venda) REFERENCES Vendas (Id_venda)
);

CREATE TABLE Matricula (
Id_turma Int,
Id_pessoa Int,
PRIMARY KEY(Id_turma,Id_pessoa),
FOREIGN KEY(Id_turma) REFERENCES Turmas (Id_turma),
FOREIGN KEY(Id_pessoa) REFERENCES Alunos (Id_pessoa)
);

CREATE TABLE Prod_Turmas (
Id_turma Int,
id_produto Int,
PRIMARY KEY(Id_turma,id_produto),
FOREIGN KEY(Id_turma) REFERENCES Turmas (Id_turma),
FOREIGN KEY(id_produto) REFERENCES Produtos (Id_produto)
);